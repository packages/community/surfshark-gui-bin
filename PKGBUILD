# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Bernhard Landauer <bernhard@manjaro.org>

# https://ocean.surfshark.com/debian/dists/stretch/main/binary-amd64/Packages
# https://ocean.surfshark.com/debian/dists/stretch/main/binary-arm64/Packages

pkgname=surfshark-client
pkgver=3.3.0
pkgrel=1
pkgdesc="Surfshark VPN client"
arch=('x86_64' 'aarch64')
url="https://surfshark.com/vpn"
license=('LicenseRef-unknown')
depends=(
  'alsa-lib'
  'curl'
  'dbus'
  'gjs'
  'gtk3'
  'iproute2'
  'iptables'
  'libnotify'
  'libxtst'
  'networkmanager'
  'networkmanager-openvpn'
  'nss'
  'org.freedesktop.secrets'
  'zip'
)
optdepends=(
  'libappindicator-gtk3: StatusNotifierItem support'
  'libsecret'
  'noto-fonts-emoji'
  'polkit'
  'polkit-gnome'
  'polkit-kde-agent'
)
conflicts=('surfshark-gui-bin')
replaces=('surfshark-gui-bin')
install=surfshark.install
source_x86_64=("https://ocean.surfshark.com/debian/pool/main/s/surfshark_${pkgver}_amd64.deb")
source_aarch64=("https://ocean.surfshark.com/debian/pool/main/s/surfshark_${pkgver}_arm64.deb")
sha256sums_x86_64=('7253d604927fc89163a3ea476b6453dc33a63dae4cb634b7deffeb7628dd534a')
sha256sums_aarch64=('3e9c942be23388ed385359ce9a74c5d661d89ab3ddd7c23bb3e30abaee5859a5')

package(){

  # Extract package data
  bsdtar -xvf data.tar.xz -C "${pkgdir}/"

  # Correct folder permissions
  find "${pkgdir}" -type d -exec chmod 755 {} \;

  # Correct file permissions
  chmod 755 "${pkgdir}"/opt/Surfshark/resources/dist/resources/{surfsharkd.js,surfsharkd2.js,update,diagnostics}
  chmod 644 "${pkgdir}"/var/lib/surfshark/{ovpn.cer,static.key}

  # SUID chrome-sandbox for Electron 5+
  chmod 4755 "${pkgdir}/opt/Surfshark/chrome-sandbox"

  # Link to the binary
  install -d "${pkgdir}/usr/bin"
  ln -s /opt/Surfshark/surfshark "${pkgdir}/usr/bin/"

  # Remove init.d
  rm -rf "$pkgdir/etc"
}
